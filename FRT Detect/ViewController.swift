//
//  ViewController.swift
//  FRT Detect
//
//  Created by sathish on 28/02/20.
//  Copyright © 2020 sathish. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController ,WKNavigationDelegate,WKUIDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        let webView = WKWebView(frame:CGRect(x: 0, y: 20, width: self.view.frame.size.width, height: self.view.frame.size.height-20))
//        let link = URL(string:"http://frtqa.wavelabs.in")!
//        let link = URL(string:"https://frtqa.wavelabs.in/")!
        
   let link = URL(string:"https://frtqa.wavelabs.in/")!
        webView.uiDelegate = self
        webView.navigationDelegate = self
        let request = URLRequest(url: link)
        webView.load(request)
        self.view.addSubview(webView)
       
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
    if navigationAction.request.url?.scheme == "tel" {
        
        print(navigationAction.request.url!)
        
        UIApplication.shared.openURL(navigationAction.request.url!)
        decisionHandler(.cancel)
    } else {
        decisionHandler(.allow)
    }
    }
}
